#include<stdio.h>

int attend(int p);
int revenue(int p,int n);
int cost(int n);
int profit(int p,int n);

int attend(int p){
	/*Formula to calculate the number of attendees for a given ticket price*/
	int n;
	n = 180 -4*p;
	return n;
}

int revenue(int x,int y){
	return x*y;
}

int cost(int p){
	return 500 +(3* attend(p));
}

int profit(int x,int y){
	int pro= revenue(x,y)-cost(x);
	return pro;
}	

int main()
{
	int maxprice, i, a, b, maxprofit=0;

	/*Calculate the price at which attendees become zero*/
	maxprice = (180 + 0)/4;

	for (i=5; i<maxprice; i=i+5){
		/*Calculate attendees*/
		a=attend(i);
		
		/*Calculate the profit*/
		b=profit(i,a);

		printf("For a ticket price of %d the profit is: %d",i,b);
		printf("\n");

		if(b>maxprofit){
			maxprofit=b;
		}
	}

	printf("...........................................\n");
	printf("The maximum profit that can be made is %d.\n",maxprofit);

	return 0;
}
