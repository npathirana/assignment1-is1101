#include <stdio.h>
int main ()
{
	/*C program to find the factors of a given number*/

	int n,i;

	/*Ask user to enter a number*/
	printf ("Enter a positive integer:");
	scanf ("%d",&n);

	printf ("Factors of the number are:\n");
	
	/*Compute the factors and print them*/
	for (i=1;i<=n;i++){
		if (n%i == 0){
			printf ("%d ",i);
		}
	}

	return 0;
}
