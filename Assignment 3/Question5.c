#include <stdio.h>

int main()
{
	/*C program to print the multiplication tables from one to n based on the given ineteger n*/

	int n,i,j,product;

	printf ("Enter an integer value:");
	scanf ("%d",&n);

	for (i=1;i<=n;i++){
		for (j=1;j<=12;j++){
			product=i*j;
			printf ("%d * %d = %d\n",i,j,product);
		}
			printf ("\n");
		}
	return 0;
}
