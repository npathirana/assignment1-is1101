#include <stdio.h>

int main ()
{
	/*C program to reverse a number*/

	int n,remainder,reverse=0;

	/*Ask user to input a number*/
	printf ("Enter an integer:");
	scanf ("%d",&n);

	/*Compute the reversed number*/
	while (n != 0){
		remainder= n % 10;
		reverse= reverse*10 + remainder;
		n=n/10;
	}
	
	printf ("The reversed number is %d\n",reverse);

	return 0;
}
