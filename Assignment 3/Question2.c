#include <stdio.h>

int main ()
{
	/*C program to determine whether a given number is a prime number*/

	int n,i,count=0;
	
	/*Ask user to input a number*/

	printf ("Enter a positive integer:");
	scanf ("%d",&n);

	/*Check whether the number has any factors other than one and the number itself*/
	for (i=2;i<=n/2;i++){
	
		if (n%i == 0){
			count=1;
			break;
		}
	}

	if (count == 0)
		printf ("%d is a prime number\n",n);

	return 0;
}

