#include <stdio.h>

int main ()
{
	/*C program to calculate sum of all given positive integers until the user enter zero or a negative value*/

	int i, sum=0;

	i=0;

	/*Take input from the user and update the sum until user enters zero or a negative integer*/
	do{
		printf ("Enter a positive integer:");
		scanf ("%d",&i);
		
		if (i<=0) break;

		sum += i;
	}while (i>0);

	/*Print the sum of the positive numbers entered by the user*/
	printf ("The sum of the positive numbers entered is %d\n",sum);

	return 0;
}

