#include <stdio.h>

int main ()
{
	/*C program to take a number from the user and check whether it is odd or even*/

	int n;

	/*Ask user to enter a number*/
	printf ("Enter a number:");
	scanf ("%d",&n);

	/*Check whether the number is negative/positive/zero*/
	(n%2 == 0)? printf("Number entered is even"): printf("Number entered is odd");

	return 0;
}
