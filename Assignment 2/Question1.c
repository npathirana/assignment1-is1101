#include <stdio.h>

int main ()
{
	/*C program to take a number from the user and check whether it is positive/negative/zero*/

	int n;

	/*Ask user to enter a number*/
	printf ("Enter a number:");
	scanf ("%d",&n);

	/*Check whether the number is negative/positive/zero*/
	if (n>0)
		printf ("The number entered is positive");
	else if (n<0)
		printf ("The number entered is negative");
	else 
		printf ("You have entered zero");

	return 0;
}
