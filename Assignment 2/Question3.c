#include <stdio.h>

int main ()
{
	/*C program to check whether a given character is a vowel or a consonant*/

	char c;

	printf ("Enter a letter of the English alphabet:");
	scanf ("%c",&c);

	/*Determine whether the character is a vowel or a consonant using switch case*/

	switch (c){
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			printf ("The character is a vowel\n");
			break;
		default: printf ("The character is a consonant\n");
		}

	return 0;
}
