/*C program to reverse a string entered by user*/

#include<stdio.h>
#include<string.h>
#define SIZE 100

int main()
{
	char s[SIZE];
	int i, l;

	printf("Enter a sentence:");
	fgets(s,SIZE,stdin);

	l= strlen(s);

	/*reverse the entered string letter by letter*/
	for(i=(l-1); i>=0; i--){
		printf("%c",s[i]);
	}
	printf("\n");

	return 0;
}

