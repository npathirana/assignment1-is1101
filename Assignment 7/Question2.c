/*C program to find the frequency of a given character in a string*/

#include<stdio.h>
#include<string.h>
#define SIZE 100

int main()
{
	char string[SIZE];
	char character;
	int count=0, i, l;

	printf("Enter a sentence: ");
	fgets(string, sizeof(string), stdin);

	l=strlen(string);

	printf("Enter the character you want to find the frequency of: ");
	scanf("%c", &character);

	/*count the number of times the character is repeated*/
	for(i=0 ; i<l; i++){
		if(string[i]==character)
			count++;
	}

	printf("The frequency of %c is %d.\n",character,count);

	return 0;
}

