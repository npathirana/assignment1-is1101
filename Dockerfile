FROM gcc
MAINTAINER Nethmi Pathirana
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "nethmi.pathirana@gmail.com"
RUN git config --global user.name "npathirana"
