#include<stdio.h>
#include<string.h>

struct Student{
	char fname[50];
	char subject[50];
	float marks; 
}stu[20];

int main(){
	int i,n;

	printf("Enter the number of student records that needs to be entered\n");
	scanf("%d",&n);

	if(n>4){
	printf("Enter the student information\n");

	/*storing the information about the students*/

	for (i=0; i<n; ++i){
		printf("\n");
		printf("Enter the first name of the student:");
		scanf("%s",stu[i].fname);
		printf("Enter the subject:");
	        scanf("%s",stu[i].subject);
		printf("Enter the marks:");
		scanf("%f",&stu[i].marks);
	}

	/*displaying the student information*/
	
	printf("\nRecorded student information");
	
	for (i=0; i<n; i++){
		printf("\n");
		printf("Name:%s\n",stu[i].fname);
		printf("Subject:%s\n",stu[i].subject);
       		printf("Marks:%.2f\n",stu[i].marks);
	}
	}

	else
		printf("Enter a minimum of 5 records\n");

	return 0;

}

