#include<stdio.h>
int main()
{
	int num1,num2,auxiliary;

	/*ask user to enter two numbers*/
	printf ("Enter the first number:");
	scanf ("%d",&num1);
	printf ("Enter the second number:");
	scanf ("%d",&num2);

	/*value of first number is assigned to auxiliary*/
	auxiliary=num1;

	/*value of second number is assigned to first number*/
	num1=num2;

	/*value of auxiliary is assigned to second number*/
	num2=auxiliary;

	/*display the swapped numbers*/
	printf ("After swapping value of first number is:%d\n",num1);
	printf ("After swapping value of second number is:%d",num2);

	return 0;
}
