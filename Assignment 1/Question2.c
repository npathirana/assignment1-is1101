#include<stdio.h>
int main ()
{
	float radius,Area,pie;

	/*assign the value of pie*/
	pie = 3.14;

	/*ask user to enter the radius of the disk*/
	printf ("Enter radius of the disk:");
	scanf ("%f",&radius);

	/*calculate and display area of the disk upto three decimal places*/
	Area= pie*radius*radius;
	printf ("Area of the disk is:%.3f",Area);

	return 0;
}
