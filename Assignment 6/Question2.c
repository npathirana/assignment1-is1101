#include<stdio.h>

int fibonacciSeq(int n);

int main(){
	int n, i;
	printf("Enter the number upto which the fibonacci series is to be displayed:");
	scanf("%d",&n);

	/*print the fibonacci sequence*/
	for (i=0; i<=n; i++){
		printf("%d\n",fibonacciSeq(i));	
	}
	return 0;
}

/*recursive function to obtain the fibonacci sequence*/
int fibonacciSeq(int n){
	if (n==0) return 0;
	else if (n==1) return 1;
	else return fibonacciSeq(n-1) + fibonacciSeq(n-2);
}
