#include<stdio.h>

int pattern (int n);

int main(){
	int i;

	/*Ask user to enter the number of lines to be printed in the number triangle*/
	printf("Enter the number of lines to be printed:");
	scanf("%d",&i);
	
	/*print the number triangle*/
	pattern(i);

	return 0;
}

/*recursive function to print the number triangle*/
int pattern(int n){ 
	int j;

	if(n>0)

	pattern(n-1);

	for (j=n; j>0; j--){
		printf("%d",j);
	}
	printf("\n");
}
	
